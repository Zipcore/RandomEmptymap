#include <sourcemod>
#include <sdktools>
#include <map_workshop_functions> // Adds workshop map support

int emptyChecks;

public Plugin myinfo = 
{
	name = "Random Emptymap",
	author = "Zipcore",
	description = "Random map if server is empty for 5 minutes",
	version = "1.0",
	url = "zipcore#googlemail.com"
}

ConVar cvWait;
int g_iWait;

ConVar cvCheckInterval;
float g_fCheckInterval;

Handle g_hTimer = null;

public void OnPluginStart()
{
	cvWait = CreateConVar("rem_wait", "5", "How much checks to wait before changing the map.");
	g_iWait = GetConVarInt(cvWait);
	HookConVarChange(cvWait, OnSettingChanged);
	
	cvCheckInterval = CreateConVar("rem_check_interval", "60.0", "Check interval.");
	g_fCheckInterval = GetConVarFloat(cvCheckInterval);
	HookConVarChange(cvCheckInterval, OnSettingChanged);
}

public void OnMapStart()
{
	emptyChecks = 0;
	g_hTimer = CreateTimer(g_fCheckInterval, Timer_CheckPlayerCount, _, TIMER_FLAG_NO_MAPCHANGE | TIMER_REPEAT);
}

public int OnSettingChanged(Handle convar, const char[] oldValue, const char[] newValue)
{
	if(convar == cvWait)
		g_iWait = StringToInt(newValue);
	else if(convar == cvCheckInterval)
	{
		g_fCheckInterval = StringToFloat(newValue);
		
		if(g_hTimer != null)
			CloseHandle(g_hTimer);
		
		g_hTimer = CreateTimer(g_fCheckInterval, Timer_CheckPlayerCount, _, TIMER_FLAG_NO_MAPCHANGE | TIMER_REPEAT);
	}
}

public Action Timer_CheckPlayerCount(Handle timer, any data)
{
	emptyChecks++;
	
	for (int i = 1; i < MaxClients; i++)
	{
		if (!IsClientInGame(i))
			continue;
			
		if (IsClientSourceTV(i))
			continue;
			
		if (IsFakeClient(i))
			continue;
		
		emptyChecks = 0;
		return Plugin_Continue;
	}
	
	if(emptyChecks >= g_iWait)
		RandomMap();
	
	return Plugin_Continue;
}

void RandomMap()
{
	// Load config
	Handle hStartmaps = CreateArray(128);
	
	char ConfigPath[256];
	BuildPath(Path_SM, ConfigPath, 256, "configs/random_emptymaps.txt");
	
	Handle hFile = OpenFile(ConfigPath, "r");
	char buffer[128];
	if (hFile != INVALID_HANDLE)
	{
		while (ReadFileLine(hFile, buffer, sizeof(buffer))) 
		{
			TrimString(buffer);
			PushArrayString(hStartmaps, buffer);
		}
		
		CloseHandle(hFile);
	}
	
	// Change map
	char sMap[128];
	GetArrayString(hStartmaps, GetRandomInt(0, GetArraySize(hStartmaps)-1), sMap, 128);
	LogMessage("Change map to %s (Reason: Server was empty for %d checks)", sMap, emptyChecks);
	CloseHandle(hStartmaps);
	
	ForceChangeLevel(sMap, "");
}